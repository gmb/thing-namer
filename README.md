# thing-namer: A utility for naming things

Generates a name from a randomly-picked adjective and noun pair.