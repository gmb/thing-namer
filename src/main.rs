extern crate clap;
mod words;
mod args;

use args::get_args;
use words::*;

/// Run thing-namer.
///
/// Call `get_adjective()` and `get_noun()` to generate one or more
/// two-part random(ish) names, and write the results to stdout.
fn main() {
    let arg_matches = get_args();
    let count = match arg_matches.value_of("count") {
        Some(count) => count.parse::<i32>().unwrap(),
        None => panic!("Expected to find a count, but couldn't."),
    };

    let delimiter = arg_matches.value_of("delimiter").unwrap_or(
        "Unable to get delimiter.",
    );

    let prefix = match arg_matches.value_of("prefix") {
        Some(prefix) => format!("{}{}", prefix, delimiter),
        None => "".to_owned(),
    };

    let suffix = match arg_matches.value_of("suffix") {
        Some(suffix) => format!("{}{}", delimiter, suffix),
        None => "".to_owned(),
    };

    for _ in 0..count {
        match arg_matches.value_of("letter") {
            Some(letter) => {
                println!(
                    "{}{}{}{}{}",
                    prefix,
                    get_adjective_by_letter(letter),
                    delimiter,
                    get_noun_by_letter(letter),
                    suffix,
                );
            }
            None => {
                println!(
                    "{}{}{}{}{}",
                    prefix,
                    get_adjective(),
                    delimiter,
                    get_noun(),
                    suffix
                );
            }
        }
    }
}
